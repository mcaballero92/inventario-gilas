<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class VehiculoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name(),
            'marca' => $this->faker->sentence(6, true),
            'modelo' => $this->faker->sentence(5, true),
            'color' => $this->faker->sentence(5, false),
            'anio' => $this->faker->year(),
            'cantidad' => $this->faker->randomNumber(),
            'ruedas' => 4,
            'motor' => 4,
        ];
    }
}
