@extends('dashboard')

@section('content')

    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form class="form-horizontal"
                      action="{{  route('api/vehiculos/save') }}"
                      autocomplete="off" data-redirect="{{ route('vehiculos') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control"
                               placeholder="Ingresa Nombre" name="nombre">
                    </div>
                    <div class="form-group">
                        <label for="marca">Marca</label>
                        <input type="text" class="form-control"
                               placeholder="Ingresa Marca" name="marca">
                    </div>
                    <div class="form-group">
                        <label for="modelo">Modelo</label>
                        <input type="text" class="form-control" name="modelo"
                               placeholder="Ingresa modelo">
                    </div>
                    <div class="form-group">
                        <label for="color">Color</label>
                        <input type="text" class="form-control"
                               placeholder="Ingresa Color" name="color">
                    </div>
                    <div class="form-group">
                        <label for="anio">Año</label>
                        <input type="number" class="form-control"
                               placeholder="Ingresa Año" name="anio">
                    </div>
                    <div class="form-group">
                        <label for="cantidad">Cantidad</label>
                        <input type="number" class="form-control"
                               placeholder="Ingresa Cantidad" name="cantidad">
                    </div>
                    <div class="form-group">
                        <label for="ruedas">Ruedas</label>
                        <select name="ruedas" class="form-control">
                            <option value="2">2</option>
                            <option value="4">4</option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="motor">Motor</label>
                        <input type="text" class="form-control"
                               placeholder="Ingresa Motor" name="motor">
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function () {

            $('form').on('submit', function (e) {
                clearFormErrors($(this));
                e.preventDefault();
                var redirect = $(this).attr("data-redirect");
                $.ajax({
                    type: 'post',
                    url: $(this).attr("action"),
                    data: $('form').serialize(),
                    success: function (response) {
                        if (response.responseJSON) {
                            setInputError(response.responseJSON);
                        } else {
                            window.location = redirect;
                        }
                    },
                });

            });

        });

        function clearFormErrors(form) {
            $(form).find('.has-error,.has-error-inside').each(function () {
                $(this).removeClass('has-error has-error-inside');
            });
            $(form).find('.help-block').remove();
        }
        var setInputError = function (field, errors) {
            if (typeof field === 'object') {
                for (var i in field) {
                    setInputError(i, field[i]);
                }
            } else {
                if (typeof errors === 'string') errors = [errors];
                var $elm = $('[name="' + field.replace('"', '\\"') + '"]:not([type="hidden"])');

                if (!$elm.is(':visible')) {
                    $elm.parents('tr').addClass('has-error-inside');
                }
                $elm.closest('td,.form-group').addClass('has-error');

                for (var i in errors) {
                    var $container;
                    if ($elm.attr('type') === 'radio') {
                        $container = $elm.parents('.form-group:first');
                    } else {
                        $container = $elm.parent();
                    }
                    if ($container.hasClass('input-group')) {
                        $container = $container.parent();
                    }
                    if ($container.hasClass('input-group-btn')) {
                        $container = $container.parent().parent();
                    }
                    $container.append($('<span class="help-block"></span>').text(errors[i]));
                }
            }


        };

    </script>
@endsection
