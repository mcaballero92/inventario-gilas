@extends('dashboard')

@section('content')
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <a class="btn btn-primary" href="./crear" role="button">Crear</a>
                <table id="vehiculosTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>Color</th>
                        <th>Año</th>
                        <th>Cantidad</th>
                        <th>Ruedas</th>
                        <th>Motor</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#vehiculosTable').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "./api/vehiculos/list",
                columns: [

                    {data: 'marca', name: 'marca'},
                    {data: 'modelo', name: 'modelo'},
                    {data: 'color', name: 'color'},
                    {data: 'anio', name: 'anio'},
                    {data: 'cantidad', name: 'cantidad'},
                    {data: 'ruedas', name: 'ruedas'},
                    {data: 'motor', name: 'motor'},
                ]
            });
        });


    </script>


@endsection
