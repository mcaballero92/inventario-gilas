<?php

namespace App\Repositories;

use App\Interfaces\VehiculoInterface;
use App\Models\Vehiculo;

class VehiculoRepository implements VehiculoInterface{

    public function insertVehiculo($data)
    {
        return Vehiculo::create($data);
    }

    public function listVehiculos($request)
    {
        $columnIndex = @$request['order'][0]['column'];
        $columnName = @$request['columns'][$columnIndex]['data'];
        $columnSortOrder = @$request['order'][0]['dir'];

        if (@$columnSortOrder)
            $data = Vehiculo::query()->orderBy($columnName,$columnSortOrder)->get();
        else
            $data = Vehiculo::query()->get();
        return [
            'status' => 'ok',
            'data' => $data,
        ];
    }

    public function filterVehiculos($filter)
    {
        // TODO: Implement filterVehiculos() method.
    }
}
