<?php

namespace App\Interfaces;

interface VehiculoInterface {
    public function insertVehiculo($data);
    public function listVehiculos($data);
    public function filterVehiculos($filter);
}
