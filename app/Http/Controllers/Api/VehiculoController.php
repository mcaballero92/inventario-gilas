<?php

namespace App\Http\Controllers\Api;

use App\Models\MotoCicleta;
use App\Models\Sedan;
use App\Models\VehiculoAbstract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Validator;
use App\Repositories\VehiculoRepository;

class VehiculoController extends Controller
{
    private VehiculoRepository $repository;
    public static function routes(){
        Route::prefix('vehiculos')->group(function (){
            Route::get('list', [VehiculoController::class, 'list']);
            Route::post('save', [VehiculoController::class, 'save'])->name('api/vehiculos/save');
        });
    }
    public function __construct(VehiculoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function list(Request $request)
    {
        return response()->json($this->repository->listVehiculos($request));
    }

    public function save($id = null)
    {
        $validate = [
            'nombre' => ['required'],
            'marca' => ['required'] ,
            'modelo' => ['required'] ,
            'color' => ['required'] ,
            'anio' => ['required'] ,
            'cantidad' => ['required'] ,
            'ruedas' => ['required'] ,
            'motor' => ['required'] ,
        ];

        $validation = Validator::make(request()->all(), $validate);
        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'message' => false,
                'data' => [],

                'responseJSON' => $validation->errors(),
            ], Response::HTTP_OK);
        }
        return response()->json(
            [
                'data' => $this->repository->insertVehiculo(request()->all())
            ],
            Response::HTTP_CREATED
        );
    }
}
