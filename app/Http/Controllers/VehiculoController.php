<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class VehiculoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function routes()
    {
        Route::get('vehiculos', [VehiculoController::class, 'index'])->name('vehiculos');
        Route::get('crear', [VehiculoController::class, 'crear'])->name('crear');

    }

    public function index()
    {
        return view('vehiculos.listado');
    }

    public function crear()
    {
        return view('vehiculos.formulario');
    }
}
