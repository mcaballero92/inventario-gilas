<?php

namespace App\Models;


use Faker\Provider\Uuid;
use Illuminate\Database\Eloquent\Model;

abstract class VehiculoAbstract extends Model
{
    protected $table = 'vehiculos';
    protected $fillable = [
        'nombre',
        'marca',
        'modelo',
        'color',
        'anio',
        'cantidad',
        'ruedas',
        'motor'
    ];


    public static function boot()
    {
        parent::boot();

        static::creating(function (VehiculoAbstract $vehiculo) {
            $vehiculo->makeId();
        });
    }

    public function makeUuid() {
        if (!$this->id) {
            $this->id = Uuid::uuid();
        }
    }
//    abstract public function ruedas();
//
//    abstract public function motor();
}
