<?php

namespace App\Models;

class MotoCicleta extends VehiculoAbstract
{

    public function ruedas()
    {
        $this->ruedas = 2;
    }

    public function motor()
    {
        $this->ruedas = 700;
    }
}
