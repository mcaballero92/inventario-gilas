<?php

namespace App\Models;


class Sedan extends VehiculoAbstract implements VehiculeInterface
{
    public function insertVehiculo($data)
    {
        $this->fill($data);
        $this->motor = 1200;
        $this->ruedas = 4;
        $this->save();
    }

    public function listVehiculos($filter = '')
    {
        $query = MotoCicleta::query();
        $query->where('ruedas',4);
        $query->where('motor',1200);
        $query->get();
    }
}
