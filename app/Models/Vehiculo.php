<?php

namespace App\Models;


use Faker\Provider\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    use HasFactory;
    protected $table = 'vehiculos';
    protected $fillable = [
        'nombre',
        'marca',
        'modelo',
        'color',
        'anio',
        'cantidad',
        'ruedas',
        'motor'
    ];


    public static function boot()
    {
        parent::boot();

        static::creating(function (Vehiculo $vehiculo) {
            $vehiculo->makeUuid();
        });
    }

    public function makeUuid() {
        if (!$this->id) {
            $this->id = Uuid::uuid();
        }
    }
//    abstract public function ruedas();
//
//    abstract public function motor();
}
